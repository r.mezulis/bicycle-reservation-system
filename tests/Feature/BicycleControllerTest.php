<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BicycleControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testViewStatus()
    {
        $response = $this->get('/bicycles');

        $response->assertStatus(200);
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testViewPage()
    {
        $response = $this->get('/bicycles');

        $response->assertSeeText('Elektriskie velosipēdi');
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateFormStatus()
    {
        $response = $this->get('/bicycles/create');

        $response->assertStatus(200);
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateFormPage()
    {
        $response = $this->get('/bicycles/create');

        $response->assertSeeText('Pievienot jaunu elektrisko velosipēdu');
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSave()
    {
        $this->post('/bicycles/create', ['name' => 'TestName']);

        $this->assertDatabaseHas('bicycles', [
            'name' => 'TestName'
        ]);
    }
}
