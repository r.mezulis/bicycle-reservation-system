<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EmployeeControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testViewStatus()
    {
        $response = $this->get('/employees');

        $response->assertStatus(200);
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testViewPage()
    {
        $response = $this->get('/employees');

        $response->assertSeeText('Darbinieki');
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateFormStatus()
    {
        $response = $this->get('/employees/create');

        $response->assertStatus(200);
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateFormPage()
    {
        $response = $this->get('/employees/create');

        $response->assertSeeText('Pievienot jaunu darbinieku');
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSave()
    {
        $this->post('/employees/create', [
            'name' => 'TestName',
            'email' => 'email@email.com'
            ]);

        $this->assertDatabaseHas('employees', [
            'name' => 'TestName',
            'email' => 'email@email.com'
        ]);
    }
}
