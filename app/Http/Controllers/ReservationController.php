<?php

namespace App\Http\Controllers;

use App\Models\Bicycle;
use App\Models\Employee;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    public function form($id)
    {
        $bicycle = Bicycle::find($id);
        $employees = Employee::all();
        return view('bicycle.reservationForm', [
            'bicycle' => $bicycle,
            'employees' => $employees
        ]);
    }

    public function reserve(Request $request, $id)
    {
        $request->validate([
            'date_from' => 'required|date|before:date_to',
            'date_to' => 'required|date|after:date_from',
            'employee' => 'required|exists:employees,id'
        ], [
            'date_from.required' => 'Lūdzu ievadiet rezervācijas sākuma datumu.',
            'date_from.before' => 'Rezervācijas sākumam jābūt pirms rezervācijas beigām.',
            'date_to.required' => 'Lūdzu ievadiet rezervācijas beigu datumu.',
            'date_to.after' => 'Rezervācijas beigām jābūt pēc rezervācijas sākuma.',
            'employee.required' => 'Lūdzu izvēlieties darbinieku.',
            'employee.exists' => 'Darbinieks netika atrasts.'
        ]);
        $bicycle = Bicycle::find($id);
        $bicycle->reserved_to_id = $request->get('employee');
        $bicycle->reserved_time_from = $request->get('date_from');
        $bicycle->reserved_time_to = $request->get('date_to');
        $bicycle->saveOrFail();
        return redirect('bicycles');
    }

    public function cancel($id)
    {
        $bicycle = Bicycle::find($id);
        $bicycle->reserved_to_id = null;
        $bicycle->reserved_time_from = null;
        $bicycle->reserved_time_to = null;
        $bicycle->saveOrFail();
        return redirect('bicycles');
    }
}
