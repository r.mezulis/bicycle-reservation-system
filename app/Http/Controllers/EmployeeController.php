<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function createForm()
    {
        return view('employee.create');
    }

    public function save(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:employees'
        ], [
            'name.required' => 'Lūdzu ievadiet darbinieka vārdu.',
            'email.required' => 'Lūdzu ievadiet darbinieka e-pasta adresi.',
            'email.unique' => 'Darbinieks ar šādu e-pasta adresi jau eksistē.'
        ]);
        $employee = new Employee();
        $employee->name = $request->get('name');
        $employee->email = $request->get('email');
        $employee->saveOrFail();
        return redirect('employees');
    }

    public function view()
    {
        $employees = Employee::all();
        return view('employee.view', [
            'employees' => $employees
        ]);
    }
}
