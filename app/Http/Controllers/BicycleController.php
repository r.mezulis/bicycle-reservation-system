<?php

namespace App\Http\Controllers;

use App\Models\Bicycle;
use App\Models\Employee;
use Illuminate\Http\Request;

class BicycleController extends Controller
{
    public function createForm()
    {
        return view('bicycle.create');
    }

    public function save(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:bicycles'
        ], [
            'name.required' => 'Lūdzu ievadiet nosaukumu.',
            'name.unique' => 'Elektriskais velosipēds ar šādu nosaukumu jau eksistē.'
        ]);
        $bicycle = new Bicycle();
        $bicycle->name = $request->get('name');
        $bicycle->saveOrFail();
        return redirect('bicycles');
    }

    public function view()
    {
        $bicycles = Bicycle::all();
        return view('bicycle.view', [
            'bicycles' => $bicycles
        ]);
    }
}
