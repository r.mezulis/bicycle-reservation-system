<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Bicycle
 *
 * @property int $id
 * @property string $name
 * @property int|null $reserved_to_id
 * @property string|null $reserved_time_from
 * @property string|null $reserved_time_to
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Employee|null $employee
 * @method static \Illuminate\Database\Eloquent\Builder|Bicycle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Bicycle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Bicycle query()
 * @method static \Illuminate\Database\Eloquent\Builder|Bicycle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bicycle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bicycle whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bicycle whereReservedTimeFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bicycle whereReservedTimeTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bicycle whereReservedToId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bicycle whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Bicycle extends Model
{
    public function employee()
    {
        return $this->belongsTo(Employee::class, 'reserved_to_id');
    }
}
