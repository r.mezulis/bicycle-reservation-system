### Setup instructions
Within the directory, rename .env.example to .env.
Run the following commands:
```
composer install
```

To use project with Laravel Sail run:
```
./vendor/bin/sail up -d
./vendor/bin/sail artisan migrate
```

If Laravel Sail is used you can open localhost to view the site.
