<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::prefix('employees')->group(function() {
    Route::get('/', [\App\Http\Controllers\EmployeeController::class, 'view']);
    Route::get('/create', [\App\Http\Controllers\EmployeeController::class, 'createForm'])->name('employees.create');
    Route::post('/create', [\App\Http\Controllers\EmployeeController::class, 'save']);
});

Route::prefix('bicycles')->group(function() {
    Route::get('/', [\App\Http\Controllers\BicycleController::class, 'view'])->name('bicycles.view');
    Route::get('/create', [\App\Http\Controllers\BicycleController::class, 'createForm'])->name('bicycles.create');
    Route::post('/create', [\App\Http\Controllers\BicycleController::class, 'save']);
    Route::get('/{id}/reserve', [\App\Http\Controllers\ReservationController::class, 'form'])->name('bicycles.reserve');
    Route::post('/{id}/reserve', [\App\Http\Controllers\ReservationController::class, 'reserve']);
    Route::post('/{id}/cancel', [\App\Http\Controllers\ReservationController::class, 'cancel'])->name('bicycles.cancel');
});

Route::any('{query}',
    function() { return redirect('/'); })
    ->where('query', '.*');
