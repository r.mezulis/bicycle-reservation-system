@extends('layout', ['title' => 'Pievienot jaunu elektrisko velosipēdu'])

@section('content')
    <div class="card m-3">
        <div class="card-header">
            Pievienot jaunu elektrisko velosipēdu
        </div>
        <div class="card-body">
            <form method="POST" action="create">
                @csrf
                <div class="form-group">
                    <label for="name">Nosaukums</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}">
                </div>
                @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <button type="submit" class="btn btn-primary float-right">Saglabāt</button>
            </form>
        </div>
    </div>
@endsection
