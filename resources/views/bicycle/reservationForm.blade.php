@extends('layout', ['title' => 'Rezervēt velosipēdu'])

@section('content')
    <div class="card m-3">
        <div class="card-header">
            Rezervēt velosipēdu
        </div>
        <div class="card-body">
            <form method="POST" action="reserve">
                @csrf
                <div class="form-group">
                    <label for="name">Nosaukums</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{$bicycle->name}}" disabled="true">
                </div>
                <div class="form-group">
                    <label for="date_from">Rezervēt no</label>
                    <input type="datetime-local" class="form-control" id="date_from" name="date_from" value="{{old('date_from')}}">
                </div>
                @error('date_from')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="date_to">Rezervēt līdz</label>
                    <input type="datetime-local" class="form-control" id="date_to" name="date_to" value="{{old('date_to')}}">
                </div>
                @error('date_to')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="employee">
                        Darbinieks
                    </label>
                    <select class="form-control" name="employee">
                        <option disabled selected value>Izvēlieties darbinieku</option>
                        @foreach($employees as $employee)
                            <option selected="{{old('employee') === $employee->id}}" value="{{$employee->id}}">{{$employee->name}}</option>
                        @endforeach
                    </select>
                </div>
                @error('employee')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <button type="submit" class="btn btn-success float-right">Rezervēt</button>
            </form>
        </div>
    </div>
@endsection
