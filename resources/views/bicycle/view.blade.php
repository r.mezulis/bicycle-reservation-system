@extends('layout', ['title' => 'Elektriskie velosipēdi'])

@section('content')
    <div class="card m-3">
        <div class="card-header">
            <div class="row justify-content-between">
                <h3>Elektriskie velosipēdi</h3>
                <a class="btn btn-success" href="{{\Illuminate\Support\Facades\URL::route('bicycles.create')}}">Pievienot
                    jaunu</a>
            </div>
        </div>
        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nosaukums</th>
                    <th scope="col">Darbinieka vārds</th>
                    <th scope="col">Rezervēts no</th>
                    <th scope="col">Rezervēts līdz</th>
                    <th/>
                </tr>
                </thead>
                <tbody>
                @foreach($bicycles as $bicycle)
                    <tr>
                        <th scope="row">{{$bicycle->id}}</th>
                        <td>{{$bicycle->name}}</td>
                        <td>{{$bicycle->employee ? $bicycle->employee->name : '-'}}</td>
                        <td>{{$bicycle->reserved_time_from ?? '-'}}</td>
                        <td>{{$bicycle->reserved_time_to ?? '-'}}</td>
                        <td>
                            @if(!$bicycle->employee)
                                <a href="{{ route('bicycles.reserve', ['id' => $bicycle->id]) }}"
                                   class="btn btn-primary">Rezervēt</a>
                            @else
                                <form method="POST" action="{{route('bicycles.cancel', ['id' => $bicycle->id])}}">
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Dzēst rezervāciju</button>
                                </form>
                            @endif
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection
