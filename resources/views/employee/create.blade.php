@extends('layout', ['title' => 'Pievienot jaunu darbinieku'])

@section('content')
    <div class="card m-3">
        <div class="card-header">
            Pievienot jaunu darbinieku
        </div>
        <div class="card-body">
            <form method="POST" action="create">
                @csrf
                <div class="form-group">
                    <label for="name">Vārds</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}">
                </div>
                @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="email">E-pasta adrese</label>
                    <input type="email" class="form-control" id="email" aria-describedby="emailHelp" name="email" value="{{old('email')}}">
                </div>
                @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <button type="submit" class="btn btn-primary float-right">Saglabāt</button>
            </form>
        </div>
    </div>
@endsection
