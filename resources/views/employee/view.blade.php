@extends('layout', ['title' => 'Darbinieki'])

@section('content')
<div class="card m-3">
    <div class="card-header">
        <div class="row justify-content-between">
            <h3>Darbinieki</h3>
            <a class="btn btn-success" href="{{\Illuminate\Support\Facades\URL::route('employees.create')}}">Pievienot jaunu</a>
        </div>
    </div>
    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <table class="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Vārds</th>
                <th scope="col">E-pasts</th>
            </tr>
            </thead>
            <tbody>
            @foreach($employees as $employee)
                <tr>
                    <th scope="row">{{$employee->id}}</th>
                    <td>{{$employee->name}}</td>
                    <td>{{$employee->email}}</td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
</div>

@endsection
